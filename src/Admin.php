<?php

namespace InstagramTimeline;

class Admin {

	function __construct() {

		if( function_exists('acf_add_options_page') ) {

			acf_add_options_page(
				array(
					'page_title' => 'Instagram',
					'menu_title' => 'Instagram',
					'menu_slug' => 'instagram-settings',
					'icon_url' 	=> 'dashicons-images-alt2',
				)
			);
	
		}

		// Add items page
		add_action( 'add_meta_boxes', function() {
			add_meta_box('instagram-items', 'Items', array($this, 'show_items'), 'admin.php?page=instagram-settings');
		});

		if(function_exists('acf_add_local_field_group')) {

			acf_add_local_field_group(array (
				'key' => 'group_594e2f0b713a2',
				'title' => 'Instagram Details',
				'fields' => array (
					array (
						'key' => 'field_594e2f13a024c',
						'label' => 'Instagram User ID',
						'name' => 'instagram_user_id',
						'type' => 'text',
						'instructions' => 'User ID for the username to get the items from. Get it <a href="https://smashballoon.com/instagram-feed/find-instagram-user-id/" target="_blank">here</a>.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array (
						'key' => 'field_594e2f36a024d',
						'label' => 'Instagram Token',
						'name' => 'instagram_token',
						'type' => 'text',
						'instructions' => 'Token to access the API. Follow <a href="https://github.com/adrianengine/jquery-spectragram/wiki/How-to-get-Instagram-API-access-token-and-fix-your-broken-feed" target="_blank">these steps</a>.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'instagram-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

		}

	}

	public function show_items() {
		?>
		<h2>Instagram Items</h2>
		<?php
	}

}