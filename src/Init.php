<?php

namespace InstagramTimeline;

class Init {


	// Verify if ACF pro is activated, die otherwise
	// Also install the database tables
	public function install() {
		ob_start();
		if (!is_plugin_active('advanced-custom-fields-pro/acf.php' ) and current_user_can('activate_plugins')) {
		    wp_die('Sorry, but this plugin requires <a href="https://www.advancedcustomfields.com/pro/" target="_blank">Advanced Custom Fields Pro</a>. Please install and activate it before.<br><a href="' . admin_url('plugins.php') . '">Return to plugins.</a>');
		}
		global $wpdb;
        $sql = "
	        CREATE TABLE `{$wpdb->prefix}instagram_items` (
				`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`image` text NOT NULL,
				`date` datetime NOT NULL,
				`text` text,
				`item_id` text,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        ob_end_flush();
	}

	// Instanciate everything
	public function bootstrap() { 
		new Admin();
		new InstagramApi();
	}

}