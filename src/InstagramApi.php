<?php

namespace InstagramTimeline;

require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');

class InstagramApi {

	private $username;
	private $token;

	public function get_items() {

		$user = get_field('instagram_user_id', 'option');
		$token = get_field('instagram_token', 'option');

		if(!$user || !$token) {
			return false;
		}

		$end_point = 'https://api.instagram.com/v1/users/' .$user. '/media/recent/?access_token=' . $token;
		$instagram_results = wp_remote_get($end_point);
		$data = json_decode($instagram_results['body'], true);
		$data = $data['data'];
		if($data) {
			global $wpdb;
			foreach($data as $item) {
				$db_item = $wpdb->get_row("SELECT id FROM {$wpdb->prefix}instagram_items WHERE item_id = '" . $item['id'] . "';");
				if(!isset($db_item->id)) {
					$uploads_dir = wp_upload_dir();
					$file_dir = $uploads_dir['basedir'] . '/instagram/';
					if(!is_dir($file_dir)) {
						mkdir($file_dir);
					}
					$filename = uniqid(rand(), true) . '.jpg';
					file_put_contents($file_dir	. $filename, file_get_contents($item['images']['standard_resolution']['url']));
					$wpdb->insert("{$wpdb->prefix}instagram_items", 
						array(
							'image' => $filename,
							'date' => date('Y-m-d H:i:s', $item['created_time']),
							'text' => utf8_encode($item['caption']['text']),
							'item_id' => $item['id']
						)
					);
					
				}
			}
			return $data;
		}

	}

	public function get_attachment_id_from_src($image_src) {
		global $wpdb;
		$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
		$id = $wpdb->get_var($query);
		return $id;
	}

}