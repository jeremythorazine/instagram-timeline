<?php

define('INSTAGRAMTIMELINE_PLUGIN_DIR', dirname(__FILE__));
define('INSTAGRAMTIMELINE_PLUGIN_URL', plugin_dir_url( __FILE__ ));

/* 
Plugin Name: Instagram Timeline
Plugin URI: http://www.akauk.com 
Description: A plugin to follow and import instagram items from a timeline. Global function provided to access the items and display them as you want.
Version: 0.1
Author: Jeremy Basolo
Author URI: https://jeremybasolo.com/
*/

// AUTOLOADER
spl_autoload_register(function ($class) {

    $prefix = 'InstagramTimeline\\';
    $base_dir = __DIR__ . '/src/';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);

    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }

});

// INIT
$init = new InstagramTimeline\Init();
register_activation_hook(__FILE__, array($init, 'install'));

// If the above didn't die, bootstrap everything
$init->bootstrap();


// Declare global variables to be used everywhere
function get_instagram_items($limit = -1, $offset = -1) {
    global $wpdb;
    $limit   = filter_var($limit, FILTER_VALIDATE_INT);
    $offset  = filter_var($offset, FILTER_VALIDATE_INT);
    $limitOffset = '';
    if($limit > 0 && $offset > -1) {
        $limitOffset = "LIMIT $limit OFFSET $offset";
    }
    $sql = "SELECT * FROM {$wpdb->prefix}instagram_items ORDER BY date DESC $limitOffset;" ;
    return $wpdb->get_results($sql);
}


if (! wp_next_scheduled ( 'get_instagram_items' )) {
    wp_schedule_event(time(), 'hourly', 'get_instagram_items');
}

register_deactivation_hook(__FILE__, function() {
    wp_clear_scheduled_hook('get_instagram_items');
});

add_action('get_instagram_items', function() {
    $Insta = new InstagramTimeline\InstagramApi();
    $Insta->get_items();
});
